<?php


namespace clientBuilding;

//const URL = "http://90077:LANOHE@webservice.optiplaza.net/Optiplaza.Webservice_test/Service.asmx";

class OrderClient
{
    public $login;
    public $soapClient;
    private $orderObject;
    private $soort;

    public function __construct($soort ,$username, $password = null)
    {
        $this->login = array('username' => $username, 'password' => $password);
        $this->soort = $soort;
        $this->soapClient = $this->getSoapClient($username, $password);
    }

    /*
     * This function return the soap client for the order
     *
     * @return Soapclient
     */
    public function getSoapClient($username, $password){
        // live server Optiplaza.Webservice_12.04
        $soapClient = new \SoapClient("http://webservice.optiplaza.net/Optiplaza.Webservice_12.04/Service.asmx?wsdl", array('login' => $username, 'password' => $password, 'trace' => 1, 'exceptions' => false));

        return $soapClient;
    }

    public function order($refractie, $order, $username){
        include_once 'xmlBuilding/mainBuilder.php';
        $orderObject = new \mainBuilder();

        file_put_contents(dirname(__DIR__) . '/../../../logs/'. $order['naam'].'refracties.txt', print_r($refractie, true), FILE_APPEND);
        file_put_contents(dirname(__DIR__) . '/../../../logs/'. $order['naam'].'werkbonnenBijShamir.txt', print_r($order, true), FILE_APPEND);

        $this->orderObject = $orderObject->startBuildObject($refractie , $order, $username, $this->soort);

        if ($this->soort == 'shamir'){
            return $this->orderToShamir($this->orderObject);
        }else{
            $this->orderToOptiplaza($this->orderToOptiplaza($this->orderObject));
        }
    }

    private function orderToShamir($orderObject){
        try {
            // Put in order
            $result = $this->soapClient->__soapCall('PutOrder', array('parameters' => array('order' => $this->orderObject)));

            file_put_contents(dirname(__DIR__) . '/../../../logs/orderObjectBijShamir.txt', print_r($this->orderObject, true), FILE_APPEND);
            file_put_contents(dirname(__DIR__) . '/../../../logs/FromShamir.txt', print_r($result, true), FILE_APPEND);

            if(strpos($result->PutOrderResult, 'true')){
                return array('success' => true, 'response' => $result->PutOrderResult);
            }else{
                return array('success' => false, 'response' => $result->PutOrderResult);
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return array('success' => false, 'response' => $e->getMessage());
        }
    }

    private function orderToOptiplaza($orderObject){
        // Try to put in the order
        try {
            // Put in order
            $result = $this->soapClient->__soapCall('PutOrder', array('parameters' => array('order' => $this->$orderObject)));

            if(strpos($result->PutOrderResult, 'true')){
                return array('success' => true, 'response' => $result->PutOrderResult);
            }else{
                return array('success' => false, 'response' => $result->PutOrderResult);
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return array('success' => false, 'response' => $e->getMessage());
        }
    }
}

//&lt;Order xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot;xmlns=&quot;http://www.optiplaza.org/schemas/order.xsd&quot;&gt;&lt;OorId&gt;1&lt;/OorId&gt;&lt;o&gt;90077&lt;/o&gt;&lt;OorTo&gt;3&lt;/OorTo&gt;&lt;OorReference&gt;tesing&lt;/OorReference&gt;&lt;OorReferenceNumber&gt;1234&lt;/OorReferenceNumber&gt;&lt;OorOcsGlass&gt;&lt;OrgGlassRight&gt;&lt;OogOglId&gt;15634&lt;/OogOglId&gt;&lt;OogSphere&gt;3&lt;/OogSphere&gt;&lt;OogCylinder&gt;2&lt;/OogCylinder&gt
////;&lt;OogAxis&gt;2&lt;/OogAxis&gt;&lt;OcsOrderGlassOperation&gt;&lt;OgoId&gt;0&lt;/OgoId&gt;&lt;OgoOpeId&gt;105151&lt;/OgoOpeId&gt;&lt;
/////OcsOrderGlassOperation&gt;&lt;/OrgGlassRight&gt;&lt;OrgGlassLeft&gt;&lt;OogOglId&gt;15672&lt;/OogOglId&gt;&lt;OogSphere&gt;-3&lt;
///// /OogSphere&gt;&lt;OogCylinder&gt;1&lt;/OogCylinder&gt;&lt;OcsOrderGlassOperation&gt;&lt;OgoId&gt;0&lt;/OgoId&gt;&lt;OgoOpeId&gt;
///// 300225&lt;/OgoOpeId&gt;&lt;/OcsOrderGlassOperation&gt;&lt;/OrgGlassLeft&gt;&lt;/OorOcsGlass&gt;&lt;/Order&gt;
//
// &lt;Order xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; xmlns:xsd=&quot;http://www.w3.org/2001/XMLSchema&quot; xmlns=&quot;http://www.optiplaza.org/schemas/order.xsd&quot;&gt; &lt;OorId&gt;1&lt;/OorId&gt; &lt;OorFrom&gt;90077&lt;/OorFrom&gt;
//// &lt;OorTo&gt;3&lt;/OorTo&gt; &lt;OorReference&gt;tesing&lt;/OorReference&gt; &lt;OorReferenceNumber&gt;1234&lt;/OorReferenceNumber&gt; &lt;OorOcsGlass&gt; &lt;OrgGlassRight&gt; &lt;OogOglId&gt;15634&lt;/OogOglId&gt;
//// &lt;OogSphere&gt;3&lt;/OogSphere&gt; &lt;OogCylinder&gt;2&lt;/OogCylinder&gt; &lt;OogAxis&gt;2&lt;/OogAxis&gt; &lt;OcsOrderGlassOperation&gt; &lt;OgoId&gt;0&lt;/OgoId&gt;
//// &lt;OgoOpeId&gt;105151&lt;/OgoOpeId&gt; &lt;/OcsOrderGlassOperation&gt; &lt;/OrgGlassRight&gt; &lt;OrgGlassLeft&gt; &lt;OogOglId&gt;15672&lt;/OogOglId&gt; &lt;OogSphere&gt;-3&lt;/OogSphere&gt;
//// &lt;OogCylinder&gt;1&lt;/OogCylinder&gt; &lt;OcsOrderGlassOperation&gt; &lt;OgoId&gt;0&lt;/OgoId&gt; &lt;OgoOpeId&gt;300225&lt;/OgoOpeId&gt; &lt;/OcsOrderGlassOperation&gt;
//// &lt;/OrgGlassLeft&gt; &lt;/OorOcsGlass&gt; &lt;/Order&gt;