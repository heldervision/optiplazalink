<?php


class getGlassId
{
    public function finder($order, $glas){
        require_once dirname(__DIR__) . '/../config/DBConnection.php';
        $dbConnection = DBConnection::returnMeekroObject();
        $glasboekId = $dbConnection->query("SELECT glasboekid FROM shamir_glasboek WHERE glasselectieid = %i AND {$order['bewerking']} = 1", $glas['glasselectie_id'])[0];
        return $glasboekId['glasboekid'];
    }
}