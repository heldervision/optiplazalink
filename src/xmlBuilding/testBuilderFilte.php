<?php
$soapBody = new \SoapVar("<?xml version=\"1.0\" ?>
<Order xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.optiplaza.org/schemas/order.xsd\">
  <OorId>1</OorId>
  <OorFrom>90077</OorFrom>
  <OorTo>3</OorTo>
  <OorReference>tesing</OorReference>
  <OorReferenceNumber>1234</OorReferenceNumber>
  <OorOcsGlass>
    <OrgGlassRight>
      <OogOglId>15634</OogOglId>
      <OogSphere>3</OogSphere>
      <OogCylinder>2</OogCylinder>
      <OogAxis>2</OogAxis>
      <OcsOrderGlassOperation>
        <OgoId>0</OgoId>
        <OgoOpeId>105151</OgoOpeId>
      </OcsOrderGlassOperation>
    </OrgGlassRight>
    <OrgGlassLeft>
      <OogOglId>15672</OogOglId>
      <OogSphere>-3</OogSphere>
      <OogCylinder>1</OogCylinder>
      <OcsOrderGlassOperation>
        <OgoId>0</OgoId>
        <OgoOpeId>300225</OgoOpeId>
      </OcsOrderGlassOperation>
    </OrgGlassLeft>
  </OorOcsGlass>
</Order>
", XSD_STRING, NULL, NULL, 'order');
$stringBody = "<Order xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.optiplaza.org/schemas/order.xsd\">
  <OorId>2</OorId>
  <OorFrom>90077</OorFrom>
  <OorTo>3</OorTo>
  <OorReference>tesing</OorReference>
  <OorReferenceNumber>1234</OorReferenceNumber>
  <OorOcsGlass>
    <OrgGlassRight>
      <OogOglId>15634</OogOglId>
      <OogSphere>3</OogSphere>
      <OogCylinder>2</OogCylinder>
      <OogAxis>2</OogAxis>
      <OcsOrderGlassOperation>
        <OgoId>0</OgoId>
        <OgoId>105151</OgoId>
      </OcsOrderGlassOperation>
    </OrgGlassRight>
    <OrgGlassLeft>
      <OogOglId>15672</OogOglId>
      <OogSphere>-3</OogSphere>
      <OogCylinder>1</OogCylinder>
      <OcsOrderGlassOperation>
        <OgoId>0</OgoId>
        <OgoOpeId>300225</OgoOpeId>
      </OcsOrderGlassOperation>
    </OrgGlassLeft>
  </OorOcsGlass>
</Order>
";
//        $soapvar  = new \SoapVar("<opticianId>90077</opticianId>", XSD_STRING);
$params = array('opticianId' => '90077');

$paramsGood = array('order' => array('OorId' => 2, 'OorFrom' => 90077, 'OorTo' => 3, 'OorReference' => 'testing', 'OorReferenceNumber' => 1234,
    'OorOcsGlass' => array('OrgGlassRight' => array('OogOglId' => 15634, 'OogSphere' => 3, 'OogCylinder' => 2, 'OogAxis' => 2,
        'OcsOrderGlassOperation' => array('OgoId' => 0, 'OgoOpeId' => 105151)), 'OrgGlassLeft' => array(
        'OogOglId' => 15672, 'OogSphere' => 3, 'OogCylinder'  => 1, 'OcsOrderGlassOperation'  => array('OgoId' => 0, 'OgoOpeId'  => 300225)))));


$company         = new \SoapVar(2, XSD_INT, null, null, 'OorId');
$computer        = new \SoapVar(90077, XSD_INT, null, null, 'OorFrom');
$facility        = new \SoapVar(3, XSD_INT, null, null, 'OorTo');
$reference = new \SoapVar('testing' , XSD_STRING, null, null, 'OorReference');

$OogOglId         = new \SoapVar(15634, XSD_INT, null, null, 'OogOglId');
$OogSphere       = new \SoapVar(3, XSD_INT, null, null, 'OogSphere');
$OogCylinder        = new \SoapVar(2, XSD_INT, null, null, 'OogCylinder');
$OogAxis =         new \SoapVar(2 , XSD_INT, null, null, 'OogAxis');

$OgoId        = new \SoapVar(0, XSD_INT, null, null, 'OgoId');
$OgoOpeId     = new \SoapVar(105151, XSD_INT, null, null, 'OgoOpeId');

$OcsOrderGlassOperation = new \SoapVar(array($OgoId, $OgoOpeId), SOAP_ENC_OBJECT, '', '', 'OcsOrderGlassOperation');

$glasRechts = new \SoapVar(array($OogOglId, $OogSphere, $OogCylinder, $OogAxis, $OcsOrderGlassOperation), SOAP_ENC_OBJECT, '', '', 'OrgGlassRight');

$OogOglId         = new \SoapVar(15672, XSD_INT, null, null, 'OogOglId');
$OogSphere       = new \SoapVar(-3, XSD_INT, null, null, 'OogSphere');
$OogCylinder        = new \SoapVar(1, XSD_INT, null, null, 'OogCylinder');

$OgoId        = new \SoapVar(0, XSD_INT, null, null, 'OgoId');
$OgoOpeId     = new \SoapVar(105151, XSD_INT, null, null, 'OgoOpeId');

$OcsOrderGlassOperation = new \SoapVar(array($OgoId, $OgoOpeId), SOAP_ENC_OBJECT, '', '', 'OcsOrderGlassOperation');

$glasLinks = new \SoapVar(array($OogOglId, $OogSphere, $OogCylinder, $OcsOrderGlassOperation), SOAP_ENC_OBJECT, '', '', 'OrgGlassRight');

$oorOCSGlass = new \SoapVar(array($glasRechts, $glasLinks), SOAP_ENC_OBJECT, '', '', 'OorOcsGlass');

$orderObject = new \SoapVar(array($company, $computer, $facility, $reference, $oorOCSGlass), SOAP_ENC_OBJECT, '', '', 'order');



$params = array('OorId' => 22, 'OorFrom' => 90077, 'OorTo' => 3, 'OorReference' => 'testiSng', 'OorReferenceNumber' => 12234,
    'OorOcsGlass' => array('OrgGlassRight' => array('OogOglId' => 15634, 'OogSphere' => 3, 'OogCylinder' => 2, 'OogAxis' => 2,
        'OcsOrderGlassOperation' => array('OgoId' => 0, 'OgoOpeId' => 105151)), 'OrgGlassLeft' => array(
        'OogOglId' => 15672, 'OogSphere' => -3, 'OogCylinder'  => 1, 'OcsOrderGlassOperation'  => array('OgoId' => 0, 'OgoOpeId'  => 300225))));



function array2xml($data, $name='root', &$doc=null, &$node=null){
    if ($doc==null){
        $doc = new \DOMDocument();
        $doc->formatOutput = FALSE;
        $node = $doc;
    }

    if (is_array($data)){
        foreach($data as $var=>$val){
            if (is_numeric($var)){
                \clientBuilding\array2xml($val, $name, $doc, $node);
            }else{
                if (!isset($child)){
                    $child = $doc->createElement($name);
                    $node->appendChild($child);
                }

                array2xml($val, $var, $doc, $child);
            }
        }
    }else{
        $child = $doc->createElement($name);
        $node->appendChild($child);
        $textNode = $doc->createTextNode($data);
        $child->appendChild($textNode);
    }

    if ($doc==$node) return $doc->saveXML();
}

//        print_r(array2xml($params, 'order'));
$test = array2xml($params, 'orders');
$test = substr($test, 29);
//        $test = substr($test, 1, -10);
//        print_r($test);
//        print_r($paramsGood);


$xmlorder = '<Order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.optiplaza.org/schemas/order.xsd">
  <OorId>1</OorId>
  <OorFrom>90077</OorFrom>
  <OorTo>3</OorTo>
  <OorReference>tesing</OorReference>
  <OorReferenceNumber>1234</OorReferenceNumber>
  <OorOcsGlass>
    <OrgGlassRight>
      <OogOglId>15634</OogOglId>
      <OogSphere>3</OogSphere>
      <OogCylinder>2</OogCylinder>
      <OogAxis>2</OogAxis>
      <OcsOrderGlassOperation>
        <OgoId>0</OgoId>
        <OgoOpeId>105151</OgoOpeId>
      </OcsOrderGlassOperation>
    </OrgGlassRight>
    <OrgGlassLeft>
      <OogOglId>15672</OogOglId>
      <OogSphere>-3</OogSphere>
      <OogCylinder>1</OogCylinder>
      <OcsOrderGlassOperation>
        <OgoId>0</OgoId>
        <OgoOpeId>300225</OgoOpeId>
      </OcsOrderGlassOperation>
    </OrgGlassLeft>
  </OorOcsGlass>
</Order>';


$xmlorder = str_replace("\n", '', $xmlorder);
$xmlorder = str_replace("\t", '', $xmlorder);
$xmlorder = str_replace("  ", '', $xmlorder);
//        $xmlorder = htmlspecialchars($xmlorder);

$orderObject = str_replace("\n", '', $orderObject);
$orderObject = str_replace("\t", '', $orderObject);
$orderObject = str_replace("  ", '', $orderObject);
