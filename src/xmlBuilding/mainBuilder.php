<?php


class mainBuilder
{
    private $soort;
    // In this function we will build the xml object piece bij piece
    public function startBuildObject($refractie, $order, $username, $soort)
    {
        require_once 'extraInformationXml.php';
        require_once 'getGlassId.php';

        $this->soort = $soort;

        $orderObject = $this->buildHeader($order, $username, 'shamir');

        $orderObject = $orderObject . $this->buildMainPart($order, $refractie);

        $orderObject = $this->deleteSpaces($orderObject);

        return $orderObject;
    }

//    This Function will build the main part of the XML body with the glasses information
    public function buildMainPart($order, $refractie) {
        $extraInformation = new extraInformationXml();
        $getGlasID = new getGlassId();
        $inslijpHoogte = $extraInformation->inslijpHoogteRekenen($order);
        $opmerking = isset($order['opmerking']) ? $order['opmerking'] : $order['werkplaatsid'];

        if($order['glaslinksrechts'] == 'Beide'){
            $rightEye = "<OrgGlassRight><OogOglId>". $getGlasID->finder($order, isset($order['glasRechts']) ? $order['glasRechts'] : $order['glasLinks']) ."</OogOglId><OogSphere>".number_format($refractie['rsferich'], 2)."</OogSphere><OogCylinder>".number_format($refractie['rcylinder'],2)."</OogCylinder>". "<OogAxis>".floatval($refractie['ras'])."</OogAxis>". $extraInformation->getAdditie($refractie, 'r', $order) . $extraInformation->getPrism($refractie, 'r') . $extraInformation->getBasis($refractie, 'r') . $inslijpHoogte['hoogteRechts']."<OogCentreDistance>".$order['pdrechts']."</OogCentreDistance>" . $extraInformation->extraRefractionInformation($refractie, 'r', $this->soort) . $extraInformation->randdikte($order, $refractie,'r').  $extraInformation->neusmaat($order) . '<OogMessage>'. $opmerking .'</OogMessage>' . $extraInformation->getDetailsGlass($getGlasID->finder($order, isset($order['glasRechts']) ? $order['glasRechts'] : $order['glasLinks']), isset($order['glasRechts']) ? $order['glasRechts'] : $order['glasLinks'], $order, $this->soort) .  $this->doorbuiging($order).  "</OrgGlassRight>";
            $leftEye = "<OrgGlassLeft><OogOglId>". $getGlasID->finder($order, $order['glasLinks']) ."</OogOglId><OogSphere>".number_format($refractie['lsferich'], 2)."</OogSphere><OogCylinder>".number_format($refractie['lcylinder'], 2)."</OogCylinder><OogAxis>".$refractie['las']."</OogAxis>". $extraInformation->getAdditie($refractie, 'l', $order) . $extraInformation->getPrism($refractie, 'l') . $extraInformation->getBasis($refractie, 'l') . $inslijpHoogte['hoogteLinks']."<OogCentreDistance>".$order['pdlinks']."</OogCentreDistance>" . $extraInformation->extraRefractionInformation($refractie, 'l', $this->soort) . $extraInformation->randdikte($order, $refractie, 'l') .  $extraInformation->neusmaat($order) .  '<OogMessage>'. $opmerking .'</OogMessage>' . $extraInformation->getDetailsGlass($getGlasID->finder($order, $order['glasLinks']), $order['glasLinks'], $order, $this->soort). $this->doorbuiging($order) . "</OrgGlassLeft>";
        }elseif ($order['glaslinksrechts'] == 'Links'){
            $rightEye = "";
            $leftEye = "<OrgGlassLeft><OogOglId>". $getGlasID->finder($order, $order['glasLinks']) ."</OogOglId><OogSphere>".number_format($refractie['lsferich'], 2)."</OogSphere><OogCylinder>".number_format($refractie['lcylinder'], 2)."</OogCylinder><OogAxis>".$refractie['las']."</OogAxis>". $extraInformation->getAdditie($refractie, 'l', $order) . $extraInformation->getPrism($refractie, 'l') . $extraInformation->getBasis($refractie, 'l') . $inslijpHoogte['hoogteLinks']."<OogCentreDistance>".$order['pdlinks']."</OogCentreDistance>" . $extraInformation->extraRefractionInformation($refractie, 'l', $this->soort) . $extraInformation->randdikte($order, $refractie, 'l') .  $extraInformation->neusmaat($order) .  '<OogMessage>'. $opmerking .'</OogMessage>' . $extraInformation->getDetailsGlass($getGlasID->finder($order, $order['glasLinks']), $order['glasLinks'], $order, $this->soort). $this->doorbuiging($order) . "</OrgGlassLeft>";
        }elseif ($order['glaslinksrechts'] == 'Rechts'){
            $rightEye = "<OrgGlassRight><OogOglId>". $getGlasID->finder($order, isset($order['glasRechts']) ? $order['glasRechts'] : $order['glasLinks']) ."</OogOglId><OogSphere>".number_format($refractie['rsferich'], 2)."</OogSphere><OogCylinder>".number_format($refractie['rcylinder'],2)."</OogCylinder>". "<OogAxis>".floatval($refractie['ras'])."</OogAxis>". $extraInformation->getAdditie($refractie, 'r', $order) . $extraInformation->getPrism($refractie, 'r') . $extraInformation->getBasis($refractie, 'r') . $inslijpHoogte['hoogteRechts']."<OogCentreDistance>".$order['pdrechts']."</OogCentreDistance>" . $extraInformation->extraRefractionInformation($refractie, 'r', $this->soort) . $extraInformation->randdikte($order, $refractie,'r').  $extraInformation->neusmaat($order) . '<OogMessage>'. $opmerking .'</OogMessage>' . $extraInformation->getDetailsGlass($getGlasID->finder($order, isset($order['glasRechts']) ? $order['glasRechts'] : $order['glasLinks']), isset($order['glasRechts']) ? $order['glasRechts'] : $order['glasLinks'], $order, $this->soort) .  $this->doorbuiging($order).  "</OrgGlassRight>";
            $leftEye = "";
        }

        $body = $rightEye . $leftEye;

        //Add shapeId to order
        $body = $this->soort == 'shamir' ? $body . $this->getOrgTrace($order) : $body;
        $body = $this->soort == 'shamir' ? $body . $this->addStoreId($order) : $body;

        return "<OorOcsGlass>".$body."</OorOcsGlass>";
    }

    // geef terug te org trace
    private function getOrgTrace($order){
        $shapeId = isset($order['shapeId_old']) ? $order['shapeId_old'] : $order['shapeId'];
        return $order['ordertype'] == 'vrijevorm' ? '<OrgTrace>'.base64_encode(1891842).'</OrgTrace>' : '<OrgTrace>'.base64_encode($shapeId).'</OrgTrace>';
    }

    // Voeg winkel locatie toe
    private function addStoreId($order){
        return '<OrgOMATrace>'.base64_encode($order['OmlId']).'</OrgOMATrace>';
    }

//  Kijk of doorbuiging nodig is bij bestelling
    private function doorbuiging($order, $doorbuiging = null){
        if (strtolower($order['focaal']) == 'multifocaal' or !strpos(strtolower($order['glasLinks']['naam']), 'urban')){
            if (isset($order['doorbuigingWerkplaats'])) {
                $doorbuiging = $order['doorbuigingWerkplaats'] > 100 ? floatval($order['doorbuigingWerkplaats'] / 100) : $order['doorbuigingWerkplaats'];
            }else{
                $doorbuiging = $order['doorbuiging'] > 100 ? floatval($order['doorbuiging'] / 100) : $order['doorbuiging'];
            }
        }

        return  $doorbuiging > 0 ? '<OogFrameCurve>'.$doorbuiging.'</OogFrameCurve>' : '';
    }

    private function deleteSpaces($orderObject){
        $orderObject = str_replace("\n", '', $orderObject);
        $orderObject = str_replace("\t", '', $orderObject);
        $orderObject = str_replace("  ", '', $orderObject);

//      And complete order Object
        $orderObject = $orderObject . "</Order>";

        return $orderObject;
    }

    private function buildHeader($order, $username, $soort){
        $referentie = $order['naam'][0] . $order['vestigingsid'] . $order['werkplaatsid'];

        if($soort == 'shamir'){
            return '<Order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.optiplaza.org/schemas/order.xsd"><OorId>'. $order['orderid'] . rand(0,999) .'</OorId><OorFrom>'.$username.'</OorFrom><OorTo>22</OorTo><OorReference>'. $referentie .'</OorReference><OorReferenceNumber>'. $order['klantid'] .'</OorReferenceNumber>';
        }else{
            return '<Order xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.optiplaza.org/schemas/order.xsd"><OorId>'. $order['orderid'] . rand(0,999) .'</OorId><OorFrom>'. $username .'</OorFrom><OorTo>3</OorTo><OorReference>'. $referentie .'</OorReference><OorReferenceNumber>'. $order['klantid'] .'</OorReferenceNumber>';
        }
    }
}
/*
 * Referentie = Letterstad + vestigingsid + werkplaatsbon
 */