<?php


class extraInformationXml
{
//  Look what information is filled in on the refracties and put it here
    public function extraRefractionInformation($refractie, $side, $soort, $extraInformation = ''){
//       Standaard waarden die we meegeven bij shamir
        if ($soort == 'shamir'){
            $extraInformation = $extraInformation . '<OogInclinationAngle><OogInclinationAngleValue>5</OogInclinationAngleValue><OogInclinationAngleUnit>0</OogInclinationAngleUnit></OogInclinationAngle><OogCorneaDistance>12.0</OogCorneaDistance>';
        }

        return $extraInformation;
    }

    public function neusmaat($order){

        if (strpos($order['montuurtype'], 'randloos') !== false or strpos($order['montuurtype'], 'nylor') !== false){
            $neusmaat = $order['neusmaat'] < 50 ? '<OogNoseWidth>'. $order['neusmaat'] * 100 .'</OogNoseWidth>' : '<OogNoseWidth>'.$order['neusmaat'] / 100 .'</OogNoseWidth>';
        }

        return '';
    }

    public function getPrism($refractie, $side){
        if($refractie[$side. 'prisma'] <> 0){
            return "<OogPrism1>".$refractie[$side. 'prisma']."</OogPrism1>";
        }
    }

    public function getBasis($refractie, $side){
        //basis
        if($refractie[$side. 'basis'] <> 0){
            return "<OogPrismAxis1>".$refractie[$side. 'basis']."</OogPrismAxis1>";
        }
    }

    public function getAdditie($refractie, $side, $order){
        //additie
        if($refractie[$side. 'additie'] <> 0 and (strtolower($order['focaal']) == 'multifocaal' or strtolower($order['focaal']) == 'bifocaal')){
            $additie = "<OogAddition>".floatval($refractie[$side. 'additie'])."</OogAddition>";
            return $additie;
        }
    }
//    Calculate inslijphoogte
    public function inslijpHoogteRekenen($order, $inslijpRechts = null, $inslijpLinks = null){
        if ($order['focaal'] == "multifocaal"){
            if ($order['montuurtype'] == "volrand kunststof" || @$order['bmontuurtype'] == "volrand kunststof"){
                if(strpos($order['glasLinks']['naam'], 'FreeFrame') !== false || strpos($order['glasLinks']['naam'], 'fixed') !== false || strpos($order['glasLinks']['naam'], 'Attitude') !== false || strpos($order['glasLinks']['naam'], 'Genesis') !== false){ // check for glasses to change inslijphoogte
                    $inslijpRechts = $order['hoogterechts'] - 1.0;
                    $inslijpLinks = $order['hoogtelinks'] - 1.0;
                }else{
                    $inslijpRechts = $order['hoogterechts'] - 2;
                    $inslijpLinks = $order['hoogtelinks'] - 2;
                }
            } else {
                if(strpos($order['glasLinks']['naam'], 'FreeFrame') !== false || strpos($order['glasLinks']['naam'], 'fixed') !== false || strpos($order['glasLinks']['naam'], 'Attitude') !== false || strpos($order['glasLinks']['naam'], 'Genesis') !== false){ // check for glasses to change inslijphoogte
                    $inslijpRechts = $order['hoogterechts'] - 1.5;
                    $inslijpLinks = $order['hoogtelinks'] - 1.5;
                }else{
                    $inslijpRechts = $order['hoogterechts'] - 2.5;
                    $inslijpLinks = $order['hoogtelinks'] - 2.5;
                }
            }
        }

        if($order['focaal'] == 'relax'){
            if(strpos($order['montuurtype'], 'metaal') !== false || strpos(@$order['bmontuurtype'], 'metaal') !== false){
                $inslijpRechts = $order['hoogterechts'] - 3.5;
                $inslijpLinks = $order['hoogtelinks'] - 3.5;
            }else{
                $inslijpRechts = $order['hoogterechts'] - 3;
                $inslijpLinks = $order['hoogtelinks'] - 3;
            }
        }

        if ($order['focaal'] == 'unifocaal'){
            $inslijpRechts = $order['hoogterechts'];
            $inslijpLinks = $order['hoogtelinks'];
        }
//      Plus 4 omdat dit ook moet voor johan in het bestel system
        if ($order['focaal'] == 'bifocaal'){
            $inslijpRechts = $order['hoogterechts'] + 4;
            $inslijpLinks = $order['hoogtelinks'] + 4;
        }

        return array('hoogteRechts' => '<OogSegmentHeight>'.$inslijpRechts.'</OogSegmentHeight>', 'hoogteLinks' => '<OogSegmentHeight>'.$inslijpLinks.'</OogSegmentHeight>');
    }

    //   This function is to get OcsOrderGlassOperation and look they are neccesarry and return glasID
    public function getDetailsGlass($glasid, $glas, $order, $soort, $ocsOperations = '', $count = 0){
        //Import connection to DB
        if (!isset(DB::$host)){
            require_once '../config/DBConnection.php';
        }else{
            DB::useDB('glasboeken');
        }

        if($soort == 'shamir'){
            // Add Precal to List if not voorraad
            if(strpos($glas['naam'], 'voorraad') == false){
                $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>'.intval($count).'</OgoId><OgoOpeId>'.intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE naam LIKE "%precal%"')).'</OgoOpeId></OcsOrderGlassOperation>';
                $count++;
            }
            // Check for sunglasses
            if($order['bewerking'] == 'kleur' or $order['bewerking'] == 'degrade'){
                // Select degrade
                if ($order['bewerking'] == 'degrade'){
                    $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>' . intval($count) . '</OgoId><OgoOpeId>' . intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE naam LIKE %s','%Degrade ' . DB::queryFirstField('SELECT naam FROM operation_shamir WHERE kleurid = %i', $order['kleur_id']) . '%')) . '</OgoOpeId></OcsOrderGlassOperation>';
                    $count++;
                }else { // Kleur nummer
                    $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>' . intval($count) . '</OgoId><OgoOpeId>' . intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE kleurid = %i', $order['kleur_id'])) . '</OgoOpeId></OcsOrderGlassOperation>';
                    $count++;
                }

                // Op basis van percentage kleur coating bepalen.
                if(intval(filter_var(DB::queryFirstField('SELECT naam FROM operation_shamir WHERE kleurid = %i', $order['kleur_id']), FILTER_SANITIZE_NUMBER_INT)) <= 50){
                    $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>'.intval($count).'</OgoId><OgoOpeId>'.intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE naam LIKE "%glacier plus uv%"')).'</OgoOpeId></OcsOrderGlassOperation>';
                    $count++;
                }else{
                    $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>'.intval($count).'</OgoId><OgoOpeId>'.intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE naam LIKE "%glacier sun uv%"')).'</OgoOpeId></OcsOrderGlassOperation>';
                    $count++;
                }

            }else{
                // Coating
                $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>'.intval($count).'</OgoId><OgoOpeId>'.intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE naam LIKE "%glacier plus uv%"')).'</OgoOpeId></OcsOrderGlassOperation>';
                $count++;
            }

            /**
             * Check to add vormslijpen
             */
            if($order['vormslijpen'] == 1  && (!strpos($order['montuurtype'], 'metaal') && !strpos(@$order['bmontuurtype'], 'metaal'))) {     
                if(strpos($order['montuurtype'], 'volrand') !== false || strpos(@$order['bmontuurtype'], 'volrand') !== false){
                    $searchTerm = "rand";
                }elseif (strpos($order['montuurtype'], 'randloos') !== false || strpos(@$order['bmontuurtype'], 'randloos') !== false){
                    $searchTerm = "glas";
                }elseif (strpos($order['montuurtype'], 'nylor') !== false || strpos(@$order['bmontuurtype'], 'nylor') !== false) {
                    $searchTerm = "nylor";
                }

                if(isset($searchTerm)){
                    $searchTerm = "Vormslijpen $searchTerm";
                    $ocsOperations = $ocsOperations . '<OcsOrderGlassOperation><OgoId>'.intval($count).'</OgoId><OgoOpeId>'.intval(DB::queryFirstField('SELECT glassOcsOperationId FROM operation_shamir WHERE naam = %s', $searchTerm)).'</OgoOpeId></OcsOrderGlassOperation>';
                    $count++;
                }
            }   

            return $ocsOperations;
        }else{ // Hoyaa
            print_r('hoyaa');
        }
    }

    public function randdikte($order, $refractie, $soort){
        if (isset($order['randdikte'])) {
            if (strpos($order['montuurtype'], 'nylor') !== false) {
                if($refractie[$soort . 'sferich'] > 0){
                    return '<OogSpecialEdgeThickness>'.$order['randdikte'].'</OogSpecialEdgeThickness>';
                }else{
                    return '<OogSpecialCentreThickness>'.$order['randdikte'].'</OogSpecialCentreThickness>';
                }
            } else {
                return '<OogSpecialCentreThickness>'.$order['randdikte'].'</OogSpecialCentreThickness>';
            }
        }

    }
}