<?php

namespace setupOptiplaza;
use clientBuilding\OrderClient;

require_once 'OrderClient.php';


class setupHandler
{
    protected $username;
    protected $password;
    //This is used to set the apikey var and give back the validation function
    public function __construct($soort, $username, $password = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->orderFunctions = new OrderClient($soort, $username, $password);
    }

    public static function create($soort, $username, $password)
    {
        return new self($soort, $username, $password);
    }
}
